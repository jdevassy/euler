#include <iostream>
#include <vector>

int sum_divisors(int num)
{
int sum=0;
for(int i=1; i<num; i++)
  {
    if(num%i == 0)
           sum+=i;
  }
return sum;
}

bool is_abundant(int num)
{
if(sum_divisors(num) > num)
 return true;
else
 return false;
}

long int sum_of_nonabundant_sums()
{
std::vector<int> abundant;
bool mark[28123];
for(int i=0; i<28123; i++)
  mark[i] = true;

for(int i=0; i<28123; i++)
{
  if(is_abundant(i))
      abundant.push_back(i);
}
for(int i=0; i<abundant.size(); i++)
{
  for(int j=0; j<abundant.size(); j++)
  {
    if(abundant[i] + abundant[j] < 28123)
        mark[abundant[i]+abundant[j]] = false;
 }
}

long int sum = 0;
for(int i=0; i<28123; i++)
{
 if(mark[i]==true) 
   sum += i;
}

return sum;
}

int main(int argc, char** argv)
{
std::cout << sum_of_nonabundant_sums() << std::endl;
return 0;

}