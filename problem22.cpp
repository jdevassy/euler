#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>

long long int sorting_names(std::vector<std::string> &list_names)
{
long long int score = 0;
long int place_score = 0;
std::sort(list_names.begin(), list_names.end());

for(std::vector<std::string>::iterator iter=list_names.begin(); iter!=list_names.end(); ++iter)
{
  std::string name = (*iter);
  long int name_score = 0;
  for(int i=0; i<name.size(); i++)
   {
   name_score += name[i] - 64;
   }
  place_score++;
  score += (name_score * place_score);
}
return score;
}

std::vector<std::string> split(const std::string &name, char delim)
{
  std::vector<std::string> list_names;
  std::stringstream ss(name);
  std::string item; 
  while(std::getline(ss, item, delim))
   { 
   list_names.push_back(item);
   }
return list_names;
}


int main(int argc, char** argv)
{
std::ifstream data("names.txt");
std::string name;
std::vector<std::string> list_names;
long long int score=0;
data >> name;
list_names = split(name, ',');
score = sorting_names(list_names);
std::cout << score << std::endl;
return 0;
}