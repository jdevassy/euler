def factorial(a):
   if (a==1):
       return 1
   if (a==0):
       return 1
   else:
       return a*factorial(a-1)

def sum_of_digits(num):
   sum = 0
   while(num > 0):
      sum += num%10
      num /= 10
   return sum

num = factorial(100)
print sum_of_digits(num)
