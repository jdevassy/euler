#include <iostream>

int collatz(long int num)
{
  int count = 0;
  while(num>1)
 {
  if(num%2 == 0)
    {num /= 2;}
  else
    {num = (3*num) + 1; }
  count++;
  }
  return count+1;
}

int main(int argc, char** argv)
{
  int max_count = 0;
  int max_start = 0;
   for(long int i=1; i<1000000; i++)
    {
      int curr_count  = collatz(i);
      if(curr_count > max_count) 
	{	max_count = curr_count;
	  max_start = i; }
    }
  std::cout << max_start << std::endl; 
  return 0;
}
