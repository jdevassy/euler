#include <iostream>

int count_Sundays()
{
int days = 1;
int days_passed = 2;
int count = 0;

for(int i=1901; i<=2000; i++)
{
  for(int j=0; j<=11; j++)
  {
    if(j==0 || j == 2 || j == 4 || j == 6 || j == 7 || j == 9 || j == 11)
    days = 31;
    if(j==1)
    {
    if(i%400==0 || (i%4 == 0 && i%100!=0)) {days = 29;}
    else {days = 28;}
    }
    if(j == 3||j ==5 || j==8|| j==10)
    days= 30;
    if(days_passed%7==0)
    count++;
    days_passed +=days;
  }
}
  return count;
}

int main(int argc, char** argv)
{
std::cout << count_Sundays() << std::endl;
return 0;
}
