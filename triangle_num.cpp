#include <iostream>


int div(long int trinum)
{
  int count = 0; //number itself 
  for(long int i=2; i*i<= trinum; i++)
    {
      if(trinum%i==0)
	{
	  count += 2;  
       }
      if(i*i == trinum) 
          count--;
    }
  
  return count;
}


long int gen_triangle_num()
{
  long int trinum=0;
  for(long int i=0; ;i++)
    {
      trinum += i+1;
      if(div(trinum) > 500)
	return trinum;
    }
}

int main(int argc, char** argv)
{
  long int trinum = gen_triangle_num();
  std::cout << trinum << std::endl;
  return 0;
}
