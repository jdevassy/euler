
def decomp(thousands, hundreds, tens, ones):

   ht_ones = {'1':'one', '2':'two', '3':'three', '4':'four', '5':'five', '6':'six', '7':'seven', '8':'eight', '9':'nine', '10':'ten', '11':'eleven', '12': 'twelve', '13':'thirteen', '14':'fourteen', '15':'fifteen', '16':'sixteen', '17':'seventeen', '18':'eighteen', '19':'nineteen'}
   ht_tens = {'2':'twenty', '3':'thirty', '4':'forty', '5':'fifty', '6':'sixty', '7':'seventy', '8':'eighty', '9':'ninety'}

   str=''
   if(thousands == 1):
       str +='onethousand'
   if(hundreds > 0 and ones == 0 and tens == 0):
       str += ht_ones[hundreds.__str__()] +'hundred' 
   if(hundreds > 0 and (ones !=0 or tens !=0)):
       str += ht_ones[hundreds.__str__()] + 'hundredand'
   if(tens == 1):
       lookup = (tens * 10) + ones
       str += ht_ones[lookup.__str__()] 
   if(tens > 1):
       str += ht_tens[tens.__str__()] 
   if(ones > 0 and tens != 1):
       str += ht_ones[ones.__str__()]
   
   return str
      

str = ''
for i in range(1001):
   thousands = i/1000
   hundreds = i/100
   tens = i/10 % 10
   ones = i % 10 
   str += decomp(thousands, hundreds, tens, ones)
 

print str.__len__()
