#include <iostream>
#include <fstream>

int find_max_at(int &arr, int i)
{
  int sum1=0;
  int sum2=0;
  int sum3=0;

  if(i <= 340 && arr[i]!=-1)
    sum1 = arr[i] + arr[20+i] + arr[40+i] + arr[60+i];
  if(arr[i]!=-1 && arr[i+1]!=-1 && arr[i+2]!=-1 && arr[i+3]!=-1)
    sum2 = arr[i] + arr[i+1] + arr[i+2] + arr[i+3];
  if(arr[i]!=-1 && arr[i+21]!=-1 && arr[i+42]!=-1 && arr[i+63]!=-1)
    sum3 = arr[i]+arr[i+21]+arr[i+42]+arr[i+63];
  if(sum1 > sum2 && sum1 > sum3)
    return arr[i] * arr[20+i] * arr[40+1] * arr[60+i];
  if(sum2 > sum1 && sum2 > sum3)
    return arr[i] * arr[i+1] * arr[i+2] * arr[i+3];
  else
    return arr[i] * arr[i+21] * arr[i+42] * arr[i+63];
}



int main(int argc, char** argv)
{
  int max_prod=1; 
  int arr[400];
  int i=0;
  std::ifstream data("data2.txt");
  int num = 0; 
  while(data >> num)
    {
      arr[i] = num;
      i++;
     if(i%20==0)
       { 
         arr[i]=-1;
         i++;
       }
    }
  int prev_prod = 1; 
  int curr_prod = 0;
  for(i = 0; i< 420; i++)
    {
      curr_prod = find_max_at(arr, i);
      if(curr_prod > prev_prod) 
	{
	  prev_prod = curr_prod; 
        }     
    }
  std::cout << prev_prod <std::endl;
  return 0; 
}
