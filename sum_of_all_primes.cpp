#include <iostream>

bool isprime(long int num)
{
  bool flag = true;
  if(num==2) return true;
      for(long int i=2; i*i <= num; i++)
	{
	  if(num%i == 0)
	    return false;
	} 
  return true;
}

long int sum_primes()
{
  long int sum = 0;
  for(long int i=2; i<2000000; i++)
    {
      if(isprime(i))
        {
          sum+=i;
	}
 }
  return sum;
}

int main(int argc, char** argv)
{
  std::cout << sum_primes() << std::endl;
}
