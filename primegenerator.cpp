#include <iostream>
#include <vector>
#include <cmath>

int main(int argc, char** argv)
{
std::vector<int> primes;

primes.push_back(2); 
primes.push_back(3);
primes.push_back(5);
int i=6;
bool flag = false;

while(primes.size()<=10000)
{
if( (i%5)!=0 && (i%3)!=0 && (i%2)!=0 )
{
int j=2;
bool flag = true;
while(primes[j] * primes[j] <= i && flag==true)
{
if(i%primes[j]==0) {flag = false;}
j++;
}
if(flag == true) 
primes.push_back(i);
}
i++;
}
std::cout << primes[10000] << std::endl; 
return 0;
}
