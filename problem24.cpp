#include <iostream>
#include <vector>

long int factorial(int num)
{
if(num == 0 || num == 1) return 1;
else return num*factorial(num-1);
}



long int permute(std::vector<int> &arr, long int step_size, long int result)
{
  for(int i=0; i<arr.size(); i++)
  {
  if(step_size * i <= result && step_size * (i+1) > result)
  {
   std::cout << arr[i] << " " << result << " " << step_size*i << std::endl; 
   result -= (step_size * i);
   arr.erase(arr.begin()+i);
  }
 }

return result;
}


void permute_million()
{
long int step_size = factorial(9);
long int result = 1000000;
std::vector<int> arr;
for(int i=0; i<10; i++)
  arr.push_back(i); 
for(int i=9; i>0; i--)
{
step_size = factorial(i);
result = permute(arr, step_size, result);
}
}



int main(int argc, char** argv)
{
 permute_million();
return 0;
}