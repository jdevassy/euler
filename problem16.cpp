#include <iostream>

long int sum(unsigned long long int pow)
{
  long int sumans = 0;
  while(pow > 0)
    {
      sumans += pow%10;
      pow/=10; 
    }
  return sumans;
}
int main(int argc, char** argv)
{
  unsigned long long int pow = 1;
  for(long int i=1; i<=100; i++)
    {
      pow *= 2;
    }
  std::cout << pow << std::endl;
  std::cout << sum(pow) << std::endl; 
  return 0;
}
