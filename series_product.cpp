#include <iostream>
#include <queue>
#include <fstream>
#include <cstdlib>

int main(int argc, char** argv)
{
  std::queue<int> window; 
  int curr_prod = 1;
  int max_prod = 1;
  std::string line;
  std::ifstream data("data.txt");
  std::ofstream newdata("newdata.txt");
  std::string l; 
  while(data >> l)
    {
      std::string new_l = l + ','; 
      new_data.write(newdata);
    }
  newdata.close(); 
 
  int num; 
  std::ifstream newdata("newdata.txt");
  while (newdata >> num)
    {
    if(num==0)
    {
	      while(!window.empty())
               window.pop();
    }   
    if(num!=0) 
    {
	  window.push(num);      
          if(window.size()==5)    
              curr_prod = (curr_prod * num) / window.front(); 
          else
	    curr_prod *= num; 
          if(curr_prod > max_prod) 
             max_prod =  curr_prod; 
          window.pop(); 
          }          
         
    }
    }
  while(!window.empty())
    {
      std::cout << window.front() << std::endl; 
      window.pop(); 
   }
  std::cout << max_prod << std::endl; 
  return 0; 
}
