#include <iostream>

long int sum_of_divisors(long int num)
{
long int sum=0;
sum += 1;
for(int i=2; i<num; i++)
{
if(num%i == 0)
 {
  sum +=i;
 }
}
return sum;
}

long int amicable(long int num)
{
long int ami_num = sum_of_divisors(num);
if(sum_of_divisors(ami_num) == num && ami_num > num)
{
std::cout << ami_num << " " << num << std::endl;
return ami_num+num;
}
else
return 0;
}

long int sum_of_amicable()
{
long int sum = 0;
for(long int i=0; i<10000; i++)
{
sum += amicable(i);
}
return sum;
}

int main(int argc, char** argv)
{
std::cout << sum_of_amicable() << std::endl;
return 0;
}
